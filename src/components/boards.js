import React, { Component } from 'react';
import Board from './board';
const token =
  'f3def5b8732f94070bdb9a0fe40ccc29fd2886a7bb397c50d7c2096c814d63ef';
const key = 'c0c092d5204e92257802ee6cf9c83489';

class Boards extends Component {
  state = {
    boards: []
  };
  componentDidMount() {
    fetch(
      `https://api.trello.com/1/members/naveentiwari11/boards?filter=all&fields=all&lists=none&memberships=none&organization=false&organization_fields=name%2CdisplayName&key=${key}&token=${token}`,
      {
        method: 'GET'
      }
    )
      .then(data => data.json())
      .then(data => {
        // console.log(data);
        this.setState({
          boards: data
        });
      });
  }
  render() {
    var allBoards = this.state.boards.map(board => {
      return <Board key={board.id} boards={board} />;
    });
    return allBoards;
  }
}

export default Boards;
